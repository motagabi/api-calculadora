package br.com.calculadora.calculadora.CalculadoraService;

import br.com.calculadora.calculadora.DTO.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        int resultado =0;
        for(Integer numero : calculadora.getNumeros()){
            resultado = resultado+numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO sub(Calculadora calculadora){
        int resultado =0;
        for(Integer numero : calculadora.getNumeros()){
            resultado = numero - resultado;
            if(resultado < 0 ){
                resultado = resultado *-1;
            }

        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
    public RespostaDTO multi(Calculadora calculadora){
        int resultado =0;
        for(Integer numero : calculadora.getNumeros()){
            resultado = resultado * numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
    public RespostaDTO divi(Calculadora calculadora){
        int resultado =0;
        for(Integer numero : calculadora.getNumeros()){
            resultado = resultado / numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
}
