package br.com.calculadora.calculadora.controllers;

import br.com.calculadora.calculadora.CalculadoraService.CalculadoraService;
import br.com.calculadora.calculadora.DTO.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired //coloca objeto da Service, não precisa instaciar a class c
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size()<=1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessário mais de um número");
        }
        return calculadoraService.somar(calculadora);
    }
    @PostMapping("/sub")
    public RespostaDTO sub(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size()<=1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessário mais de um número");
        }
        return calculadoraService.sub(calculadora);
    }
    @PostMapping("/multi")
    public RespostaDTO multi(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size()<=1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessário mais de um número");
        }
        return calculadoraService.multi(calculadora);
    }
    @PostMapping("/divi")
    public RespostaDTO divi(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size()<=1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessário mais de um número");
        }
        return calculadoraService.multi(calculadora);
    }
}
